import java.util.*;

/*
 * Nama: Putu Indah Githa Cahyani
 * NIM: 235150200111041
 * Kelas: TIF - D
 */

public class ProgramKonversi {
    public static void main(String[] args) {
        System.out.println("Silakan pilih konversi di bawah ini.");
        System.out.println("1. Biner ke Desimal");
        System.out.println("2. Desimal ke Biner");
        System.out.println("3. Biner ke Hexadesimal");
        System.out.println("4. Hexadesimal ke Biner");
        System.out.println("5. Desimal ke Hexadesimal");
        System.out.println("6. Hexadesimal ke Desimal");
        System.out.print("Pilihan: ");

        Scanner scanner = new Scanner(System.in);

        int choice = scanner.nextInt();
        scanner.nextLine();

        String binaryString = "";
        int decimal = 0;
        String hexaString = "";

        if (choice == 1) {
            System.out.print("Masukkan bilangan biner: ");
            binaryString = scanner.nextLine();
            decimal = Integer.parseInt(binaryString, 2);
            System.out.println("Bilangan desimal: " + decimal);
        } else if (choice == 2) {
            System.out.print("Masukkan bilangan desimal: ");
            decimal = scanner.nextInt();
            binaryString = Integer.toBinaryString(decimal);
            System.out.println("Bilangan biner: " + binaryString);
        } else if (choice == 3) {
            System.out.print("Masukkan bilangan biner: ");
            binaryString = scanner.nextLine();
            decimal = Integer.parseInt(binaryString, 2);
            hexaString = Integer.toHexString(decimal);
            System.out.println("Bilangan hexadesimal: " + hexaString);
        } else if (choice == 4) {
            System.out.print("Masukkan bilangan hexadesimal: ");
            hexaString = scanner.nextLine();
            decimal = Integer.parseInt(hexaString, 16);
            binaryString = Integer.toBinaryString(decimal);
            System.out.println("Bilangan biner: " + binaryString);
        } else if (choice == 5) {
            System.out.print("Masukkan bilangan desimal: ");
            decimal = scanner.nextInt();
            hexaString = Integer.toHexString(decimal);
            System.out.println("Bilangan hexadesimal: " + hexaString);
        } else if (choice == 6) {
            System.out.print("Masukkan bilangan hexadesimal: ");
            hexaString = scanner.nextLine();
            decimal = Integer.parseInt(hexaString, 16);
            System.out.println("Bilangan desimal: " + decimal);
        }

        scanner.close();
    }
}
